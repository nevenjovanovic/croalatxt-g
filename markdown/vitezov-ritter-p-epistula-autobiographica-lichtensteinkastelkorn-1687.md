 
# Celsissimo ac reverendissimo principi ac domino domino Carolo episcopo Olomucensi, regiae capellae Bohemiae comiti, duci, S. R. I. et Liechtensteinii principi etc. Paulus Ritter sempiternam felicitatem.

Ut nuper reliquos inter regesque ducesque,  
Quorum Nympha piam Pannona sensit opem:  
Et tibi sacratam, princeps celsissime, laurum  
Misissem paucas associando notas.  
Nescio, num placuit pro spe votoque dicantis, 5  
Numve satis tanto principe digna fuit?  
Hoc correspondens fecit tantummodo notum,  
Quod tua, quisnam sim, gratia scire volet.  
Id paucis isthic describens versibus, oro:  
Ne graviter (quia sunt vera) legendo feras. 10  
Regia Crovatis vetus urbs et libera Segnae   
Ad maris Adriacas condita perstat aquas.  
Illic sum genitus, sacro baptismatis illic   
Romani ritus fonte lavatus ego.   
Patre a Germana procedo nobilitate: 15   
Per matrem Illyridum nobile duco genus.   
Ne numerem proavos hic Marte domique potentes,   
Clarum de Luka Rittereumque genus,   
Vivit adhuc genitor, regni qui gaudet equestri   
Patriciique urbis gaudet honore status. 20  
Ante duos fratres ego natus totque sorores,   
Post ambos fratres unicus ipse modo.   
Natu etenim minimus moritur puerilibus annis:   
Vixisset! magnae nam fuit ille spei.   
Alter, qui primus sub cincta a Thrace Vienna 25   
Et caput et signum victor ab hoste tulit,   
Ante et post etiam praeclare multa patrando   
Ante et post etiam vulnera dura tulit.   
Vulneribus tandem renovatis isque strategi   
Ultima persolvit fata gerendo vices, 30   
Unicus ut modo sim matri; sed et ipse magistri   
Supra equites Slavnos munia nactus eram.   
A quo nam Zagraba (slavne sic dicitur) urbe   
Facta meis studiis Suada coronis erat,   
Inviso patriae contermina regna Croatae, 35   
Osculor et divi limina sancta Petri.   
Indeque vicinis iterum versatus ab oris   
Egregiis cupii cognitus esse viris.   
Discebam varias, sed honestas sedulus artes   
Otia namque animo non placuere meo.   
Interea Caesar generali regna diaetae,   
Sempronii fuerat quae celebrata, vocat.   
Ad quam Segniadum legati munere functus,   
Ordinis atque status nomine missus eram,   
Cuius felici demum pro fine diaetae 45   
Regnorum grates regis honore cano.   
Factus Caesaream post haec orator ad aulam   
Ad quam sesqui anno pene moratus eram.   
Hinc postquam redii, belli coepere tumultus,   
Et cincta a duro Thrace Vienna fuit. 50   
In positis Dravi Muraeque ad flumina castris   
Illyrici semper cum vicerege fui.   
Sed post (Brezencza Babocsaque arce crematis)   
Lincii ad Augustum nuntius eius eram.   
Dumque moratus ibi bis binos transigo menses, 55   
Supra equites Slavnos fio magister eques.   
Fama mihi interea fraternae nuntia mortis,   
Praesidis atque mei plena dolore volat.   
Et dum alii contra Turcos contraque rebelles   
Justa movent fortes Caesaris arma viri: 60   
Acta relaturus bano (ceu iure decebat)   
Non bene Saviacis ipse recurro plagis.   
Nam non fraterno mala sors contenta dolore   
Plura ultro voluit me subiisse mala.   
Quippe salebrosas Styriae currendo per Alpes, 65   
Cum titubo, in petram sum male lapsus equo.   
Occurrere dein plura infortunia semper:   
Retrogrado potui nec rediisse Iove.   
Ne tamen omnino castris (licet aeger) abessem,   
Me cum prorege ad castra Croata tuli. 70   
Legio quippe mei procul est mandata tribuni,   
Leucsae et Russiacis proxima facta plagis.   
Iamque Viennensem brumae sub tempus in urbem   
Veni, et tunc absens inde tribunus erat.   
Sed me per proprias hortatus saepe tabellas 75   
Iussit, ut hic, donec venerit ille, morer.   
Qui septem primo post menses appulit, at si   
Nec tunc venisset, res bene facta foret.   
Longius hic etenim, quam par fuit ille moratus.   
Cassatum regimen tempus ad hocce dolet. 80   
Ipse reformatus quoque, nec post castra secutus   
Nec redii in paternas post ea facta plagas,   
Namque requisitus fueram, qui regis ad aulam   
Regnorum patriae publica vota geram.   
Talibus ad praesens moror hic rationibus actus 85   
Unde tamen menses ante movebo duos.   
Hinc ut apostolici pergant bene commoda regni,   
Cuius ut Illyrica nobilitate fruor,   
Composui sacram certantibus atque patronis   
Laurum, quaeque illis non moritura viret. 90   
Quos inter sacro fautores, Carole, bello   
Non sunt missa tuis ultima serta comis.   
Quae quod grato animo perceperis atque benigna,   
Ipse mihi certum suadeo, fronte legas.   
Sed ne confusus turbet, precor, ordo legentem, 95   
Sparsaque diversis nomina magna locis,   
Non fuit apta satis tanto brevis hora labori,   
Nec fuit haec nostro res agitanda foro.   
Quas sed in hoc vires ars vel natura negavit,   
Has pietas supplet candida, supplet amor. 100   
Omnia non omnis: vix omnia possumus omnes:   
Communi proprium digero sorte malum.   
Primus ab instructis anagrammatis arte libellis,   
Fert hic principibus laurea serta viris;   
Imperiis, regnis, rebus communibus alter; 105   
Tertius ad belli fert sua serta duces;   
Caesareao primis aulae regnique ministris   
Quartus; ad heroum funera quintus erit.   
Hi tamen expectant meritae primordia lucis.   
Quos reliquum sequitur nominis huius opus, 110   
Res et materies lectu sat grata profecto   
Haec erit, ad quorum venerit illa manus.   
Nec primus nostrae foetus liber iste Camoenae  
Hactenus ad lucem, quem dedit illa, fuit.   
Quatuor iam Slavis Odilenja Sigetskoga libris 115  
Et Latiis Musam nam dedit usque novam.   
Plura suis matura iacent sua pignora cunis   
Instructa Ausoniis Illyricisque notis.   
Sed Maecenates quibus inde leventur ad auras,  
Esse sub hoc paucos tempore moesta dolet. 120   
Nam nec avi mihi nec genitor nummosa pararunt   
In duris illic commoda opesque plagis,   
Qui semper Marti praeclara ad gesta studentes,   
Sat sibi credebant nomen habere bonum,   
Pro quo saepe tamen passi non pauca fuere, 125   
Fortuna ut secum bellica ferre solet,   
Ipse parens semel at bis frater uterque parentis   
Ferrea Threicio carcere vincla tulit.   
Haec breviter de me mihi perscripsisse licebat:   
De me plura alii plura meisque dabunt. 130   
Quem cum nil mage, quam clarorum fama virorum   
Delectat: tales semper honore colo.   
Cumque tua audissem plures de laude loquentes,   
Indolis illustris norma quod ipse viges:   
Mox in me dixi, dux Lichtensteine, colentes 135   
Inter postremus non erit iste tuos.   
Devoti affectus hinc argumenta per illam   
Laurum conabar prima dedisse mei.   
Postquam (si liceat princeps generose) sequetur   
Nominis in laudem Musa canora tui. 140   
Quem virtus ornat, celebrant quem carmina vatum,   
Felici aeternos vivit honore dies.   
Expectans reliquo tanti mandata patroni,   
Cui me commendo. Finio scripta. Vale.  

Viennae 29. Junii 1687.
